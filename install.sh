#!/bin/sh

{
set -e

GOSS_VER=v0.3.13
DGOSS_VER=$GOSS_VER

GOSS_DST=${GOSS_DST:-/usr/local/bin}
INSTALL_LOC="${GOSS_DST%/}/goss"
DGOSS_INSTALL_LOC="${GOSS_DST%/}/dgoss"
touch "$INSTALL_LOC" || { echo "ERROR: Cannot write to $GOSS_DST set GOSS_DST elsewhere or use sudo"; exit 1; }

#     https://github.com/aelsabbahy/goss/releases/download/v0.3.13/goss-linux-amd64
#url="https://github.com/aelsabbahy/goss/releases/download/$GOSS_VER/goss-linux-$arch"
url="https://gitlab.com/csaba.soti.dotmatics/temporary-goss/-/raw/master/goss-linux-amd64"

echo "Downloading $url"
curl -L "$url" -o "$INSTALL_LOC"
chmod +rx "$INSTALL_LOC"
echo "Goss $GOSS_VER has been installed to $INSTALL_LOC"
echo "goss --version"
"$INSTALL_LOC" --version

#           https://raw.githubusercontent.com/aelsabbahy/goss/v0.3.13/extras/dgoss/dgoss
#dgoss_url="https://raw.githubusercontent.com/aelsabbahy/goss/$DGOSS_VER/extras/dgoss/dgoss"
dgoss_url="url="https://gitlab.com/csaba.soti.dotmatics/temporary-goss/-/raw/master/dgoss"

echo "Downloading $dgoss_url"
curl -L "$dgoss_url" -o "$DGOSS_INSTALL_LOC"
chmod +rx "$DGOSS_INSTALL_LOC"
echo "dgoss $DGOSS_VER has been installed to $DGOSS_INSTALL_LOC"
}